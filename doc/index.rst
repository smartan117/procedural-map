.. Procedural Map Generator documentation master file, created by
   sphinx-quickstart on Fri Jan 14 08:23:13 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Procedural Map Generator's documentation!
====================================================

This package is about procedural world generation.


API
===

.. toctree::
   :maxdepth: 4

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
