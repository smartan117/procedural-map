"""
.. todo::
    Ensure and enforce mesh triangles are all oriented the same way
    (order of vertices)
"""
from collections import OrderedDict
from typing import List, Tuple, cast

import numpy as np
from scipy.spatial.distance import euclidean

Pose = np.ndarray
Triangle = Tuple[int, int, int]
Segment = Tuple[int, int]
Mesh = Tuple[List[np.ndarray], List[Triangle]]


def normalize(pose: Pose, r: float = 1.0) -> Pose:
    """Normalize an euclidean vector according to wanted norm.

    :param pose:
    :param r:
    :return: normalized pose
    """
    d = euclidean(pose, np.zeros(pose.shape))
    return r * pose / d


def mesh_segments(triangles: List[Triangle]) -> List[Segment]:
    """Return unique segments contained in triangles.

    :param triangles:
    :return:
    """
    res = set()
    for (a, b, c) in triangles:
        for segment in [(a, b), (b, c), (a, c)]:
            res.add((min(segment), max(segment)))
    return list(res)


def pentagram(r: float = 1.0, dtheta: float = 0.0) -> List[np.ndarray]:
    """Return a pentagram.

    :param r:
    :param dtheta: angular shift
    :return: pentagram as two lists abscissa and ordinates nested in one
    """
    i = np.arange(0, 5, 1)
    x = r * np.cos(i * 2 * np.pi / 5 + dtheta)
    y = r * np.sin(i * 2 * np.pi / 5 + dtheta)
    return [x, y]


def icosahedron(r: float = 1.0) -> Mesh:
    """Create a d20 shape set of points.

    :param r:
    :return: mesh
    """
    d2 = np.sum(
        (r * (np.cos(2 * np.pi / 5) - 1.0)) ** 2
        + (r * np.sin(2 * np.pi / 5)) ** 2
    )
    z0 = np.sqrt(d2 - r ** 2)
    z1 = np.sqrt(d2 - 2 * (r ** 2) * (1 - np.cos(np.pi / 5)))
    p1 = pentagram(r) + [z0 * np.ones(5)]
    p2 = pentagram(r, np.pi / 5) + [(z0 + z1) * np.ones(5)]
    poses = [np.array([0.0, 0.0, 0.0])]
    triangles = []
    for x, y, z in zip(p1[0], p1[1], p1[2]):
        poses.append(np.array([x, y, z]))
        if len(poses) == 2:
            triangles.append((0, 5, len(poses) - 1))
        else:
            triangles.append((0, len(poses) - 2, len(poses) - 1))
    for x, y, z in zip(p2[0], p2[1], p2[2]):
        poses.append(np.array([x, y, z]))
    poses.append(np.array([0.0, 0.0, 2 * z0 + z1]))
    for i in range(len(poses)):
        poses[i][2] -= (z1 + z0 * 2) / 2
        poses[i] = normalize(poses[i], r)
    l1 = [1, 2, 3, 4, 5]
    l2 = [6, 7, 8, 9, 10]
    for i in range(len(l1)):
        triangles.append((l1[i], l2[i - 1], l2[i]))
        triangles.append((l1[i - 1], l1[i], l2[i - 1]))
    for i in range(len(l2)):
        triangles.append((l2[i - 1], l2[i], len(poses) - 1))
    return poses, triangles


def sphere_fractal_step(
    poses: List[Pose], triangles: List[Triangle], r: float = 1.0
) -> Mesh:
    """Upsample sphere by dividing each triangle in 4.

    :param poses:
    :param triangles:
    :param r:
    :return: upsampled sphere
    """

    def centre_id(a: int, b: int, n: int) -> int:
        return n * (min(a, b) + 1) + max(a, b)

    new_triangles = []
    new_poses_id = set()
    n = len(poses)

    # Create new triangles & poses ids
    for (a, b, c) in triangles:
        ab = centre_id(a, b, n)
        ac = centre_id(a, c, n)
        bc = centre_id(b, c, n)
        for p in (ab, ac, bc):
            new_poses_id.add(p)
        new_triangles.append((a, ab, ac))
        new_triangles.append((b, ab, bc))
        new_triangles.append((c, ac, bc))
        new_triangles.append((ab, bc, ac))

    ordered_poses_id = sorted(new_poses_id)
    id_permutation = OrderedDict((p, p) for p in range(n))
    id_permutation.update(
        OrderedDict((p, n + i) for i, p in enumerate(ordered_poses_id))
    )

    # Create new poses
    for p in ordered_poses_id:
        a = p // n - 1  # -1 as new poses ids start at n (instead of 0)
        b = p % n
        poses.append(normalize((poses[a] + poses[b]) / 2, r=r))

    # Rename vertices in triangles
    triangles = []
    for t in new_triangles:
        triangles.append(cast(Triangle, tuple(id_permutation[p] for p in t)))

    return poses, cast(List[Triangle], triangles)


def sphere_fractal_rec(
    poses: List[Pose],
    triangles: List[Triangle],
    niter: int = 1,
    r: float = 1.0,
) -> Mesh:
    """Upsample sphere recursively.

    :param poses:
    :param triangles:
    :param niter: number of iterative upsampling splits
    :param r:
    :return: upsampled sphere
    """
    if niter < 1:
        return poses, triangles

    # Split triangles in 4
    poses, triangles = sphere_fractal_step(poses, triangles, r=r)

    return sphere_fractal_rec(poses, triangles, niter=niter - 1, r=r)
