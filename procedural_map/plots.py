from typing import List

import matplotlib.pyplot as plt

from .geometry import Pose, Triangle, mesh_segments


def plot_mesh(poses: List[Pose], triangles: List[Triangle], style="b"):
    """Plot 3D mesh.

    :param poses:
    :param triangles:
    :param style:
    :return:
    """
    segments = list(mesh_segments(triangles))

    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")
    for (a, b) in segments:
        pa, pb = poses[a], poses[b]
        ax.plot([pa[0], pb[0]], [pa[1], pb[1]], [pa[2], pb[2]], style)

    plt.show()
