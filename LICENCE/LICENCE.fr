Copyright Nicolas Duminy, 2022

Responsables projet:
  Nicolas Duminy <smartan007@gmail.com>

Ce logiciel est un paquet pour faire de la génération procédurale de mondes.

Ce logiciel est donné en licence selon les termes de la Licence Publique de
l'Union européenne ("EUPL": European Union Public Licence), v1.2.

Vous trouverez dans ce répertoire les versions anglaise et française de cette
licence. L'EUPL est disponible dans 22 langues officielles de l'Union
européenne; pour plus d'informations, veuillez vous référer à son site web:
https://joinup.ec.europa.eu/page/eupl-text-11-12
