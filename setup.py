from setuptools import find_packages, setup

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

classifiers = [
    "Development Status :: 3 - Alpha",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
]

setup(
    name="procedural_map",
    version="0.1.0",
    author="Nicolas Duminy",
    author_email="smartan007@gmail.com",
    description="Package for Procedural Map Generation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    classifiers=classifiers,
    package_dir={"procedural_map": "procedural_map"},
    packages=find_packages(exclude=["tests*"]),
    test_suite="tests",
    install_requires=("numpy", "scipy", "matplotlib"),
    python_requires=">=3.6",
    zip_safe=False,
)
